**😄 All About IIoT-  Industrial Internet of Things 😄**  

**What is IOT & Why do we need IOT ?**

![ALT](https://gitlab.com/balanaguyashwanth/iotmodule1/-/raw/master/extras/iot.png)

The Internet of Things aims to develop internet connectivity for the daily purpose devices that can efficiently send and receive data.
These IoT devices may come in the form of your printer, thermometer, alarm clock, phone, and other daily used devices as well.
Internet of things devices connect you with on and off switch functions to the internet that makes it able to serve in a much better way to the people.

Moreover,There are many advantages of having things connected to each other. Here are a few of the benefits:

1. More data means better decisions.
2. Ability to track and monitor things.
3. Lighten the workload with automation.

**What is IIOT ?**

  - IIoT stands for the Industrial Internet of Things or Industrial IoT that  mainly refers to a large number of devices or machines are connected and through the internet the use of software tools and third platform technologies in a machine-to-machine and Internet of Things .

![ALT](https://gitlab.com/balanaguyashwanth/iotmodule1/-/raw/master/extras/iiot.jpeg)


**Main Benefits of IIoT**

  - One of the greatest benefits of the Industrial Internet of Things has to be seen in the reduction of human errors and manual labor, the increase in overall efficiency and the reduction of costs, both in terms of time and money. We also cannot forget the possible usage IIoT in quality control and maintenance.

![ALT](https://gitlab.com/balanaguyashwanth/iotmodule1/-/raw/master/extras/marketrevenue.png)
 

  - The Industrial Internet of Things is part of the Internet of Things.  The Internet of Things or IoT is mainly  large amounts of data that get collected, aggregated and shared in a meaningful way. Here again the goal is to increase the automation. In the Industrial Internet of Things, In  Industry 4.0 context whereby automation leads to a decrease of specific types of work but at the same time requires new skill sets. 

**To summarize, here are some of the key benefits of IIoT in an industry context**

  - Improved and intelligent connectivity between devices or machines.
  - Increased efficiency.
  - Cost savings.
  - Time savings.
  - Enhanced industrial safety.
 
![ALT](https://gitlab.com/balanaguyashwanth/iotmodule1/-/raw/master/extras/edit.jpg) 


**What is Industry 3.0 ?**

![ALT](https://gitlab.com/balanaguyashwanth/iotmodule1/-/raw/master/extras/3.0premuim.gif)

  - where computers and automation ruled the industrial scene. It was during this period of transformation where more and more robots were used in the processes to perform the tasks which were performed by humans.
  - In Industry 3.0, we automate processes using logic processors and information technology. These processes often operate largely without human interference, the data we stored in database and represented in excel sheets and csv files and get plotted as real time graphs etc.
  - But there is still a human aspect behind it. Where Industry 4.0 comes in is with the availability and use of vast quantities of data on the production floor, In this we will send the data to cloud.

E.g:- Use of Programmable Logic Controllers, Robots etc.

**Mainly in single sentence** :-

  - IT and computer technology are used to automate processes


**Communication Protocols of Industry 3.0** 

![ALT](https://gitlab.com/balanaguyashwanth/iotmodule1/-/raw/master/extras/protocols3.0.jpg)

  - These protocols in Industry 3,0 are optimized and send these data to the control server inside the factory. these protocols are used by sensors to send to PLC’s (i.e various controllers), 
 
**Architecture of Industry 3.0**

![ALT](https://gitlab.com/balanaguyashwanth/iotmodule1/-/raw/master/extras/architecture3.0.jpg)

**What is Industry 4.0 ?**

  - The industry 4.0 is the adoption of industry 3.0 connecting with more protocols like MQTT, RestfulAPI etc  to send the data to cloud and enhance it with smart and autonomous systems  and machine learning etc

![ALT](https://www.mau.com/hubfs/Header---Gif.gif)
 
  - Now, and into the future as Industry 4.0 computers are connected and communicate with one another to ultimately make decisions without human involvement. A combination of physical systems, the Internet of Things and the Internet of Systems make Industry 4.0 possible and the smart factory a reality. 

  - Ultimately, it's the network of these machines that are digitally connected with one another and create and share information that results in the true power of Industry 4.0.

**Architecture of Industry 4.0** 

![Industry 4.0 Architecture](https://gitlab.com/CaelestisZ/iotmodule1/-/raw/master/assignments/summary/assets/industry-4.png)


**Communication protocols of Industry 4.0** 

![ALT](https://gitlab.com/balanaguyashwanth/iotmodule1/-/raw/master/extras/4.0protocols.png)

  - The main speciality of protocols is When the internet problem is there in the factory these protocols will store the data and until unless internet is working fine and send these data to cloud 
 
**How to Convert Industry 3.0 devices to Industry 4.0**

![ALT](https://gitlab.com/CaelestisZ/iotmodule1/-/raw/master/assignments/summary/assets/instances.png)

**In these conversion some challenges we will to face mainly:-**

  - Expensive Hardware
  - Lack of documentation
  - Proprietary of PLC protocols 

**So, to solve these type of challenges** 

  - Step1: We have to identify the popular industry 3.0 devices mainly like interfaces  and protocols 
  - Step2:Study that protocols of industry 3.0 like Modbus,CANopen etc 
  - Step3:Get the data from industry 3.0 devices 
  - Step4:Using the Edge iot gateway,OPC proxy etc Send the data  to cloud 

Also, there are some libraries and protocols to convert the industry 3.0 to industry 4.0, that will get the data from industry 3.0 to send the data to cloud like AWS IOT,GCP IOT,Azure IOT etc 
 
**They are some Dashboards to analyze and show more data like** 
  - Prometheus
  - InfluxDB 
  - Grafana
  - Thingsboard


**To get alerts** 
  - Zaiper
  - Twilio


**Out of the BOX :-**

  - Industry 4.0 Today, Industry 5.0 Tomorrow
  - Industry 4.0 is the transformation of factories into IoT-enabled smart facilities that utilize cognitive computing and interconnect via cloud servers, Industry 5.0 is set to focus on the return of human hands and minds into the industrial framework.Industry 5.0 is the revolution in which man and machine combination and find ways to work together to improve the means and efficiency of production. 
 
 
 
